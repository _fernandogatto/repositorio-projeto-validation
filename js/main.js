$(document).ready(function(event){
    $('#submit').click(function(event){
        event.preventDefault();
        
        var nome = $('#nome').val();
        var endereco = $('#endereco').val();
        var telefone = $('#telefone').val();
        var qtd_valido = 0;

        if(nome.length < 1){
            alert('Entre com um nome válido');
            $('#nome').css('background-color', '#f97575');
        } else {
            $('#nome').css('background-color', '#9cd879');
            qtd_valido += 1;
        }

        if(endereco.length < 1){
            alert('Entre com um endereço válido');
            $('#endereco').css('background-color', '#f97575');
        } else {
            $('#endereco').css('background-color', '#9cd879');
            qtd_valido += 1;
        }

        if(telefone.length < 9){
            alert('Entre com um telefone válido');
            $('#telefone').css('background-color', '#f97575');
        } else {
            $('#telefone').css('background-color', '#9cd879');
            qtd_valido += 1;
        }
        
        if(qtd_valido == 3){
            $('#topico-mostragem').append('<div><h2>' + nome + '</h2><p>' + endereco +'</p><p>' + telefone +'</p></div>');
        }
        
    })

});